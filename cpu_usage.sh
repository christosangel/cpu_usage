#! /bin/bash
#  
#┏━╸┏━┓╻ ╻   ╻ ╻┏━┓┏━┓┏━╸┏━╸
#┃  ┣━┛┃ ┃   ┃ ┃┗━┓┣━┫┃╺┓┣╸ 
#┗━╸╹  ┗━┛   ┗━┛┗━┛╹ ╹┗━┛┗━╸  
#     
#A bash script written by Christos Angelopoulos, September 2021
#An applet for Cinnamon DE, to be used with panel applet ComandRunner
cache=/tmp/cpubarscache
stats="$(grep cpu /proc/stat)"
if [ ! -f $cache ]
then 
 echo "$stats" > "$cache"
	sleep 1
	stats="$(grep cpu /proc/stat)"
fi
old=$(cat "$cache")
TT="$(echo "$stats"&& echo "$old")"
MM="$(echo "DISK : "$(df |grep /dev/sda2|awk '{print $5}'))"
R0="$(echo "RAM :"$(free -h|grep "Mem"|awk '{print $3}'))"
R="$(echo "Occupied RAM: "$(free -h|grep "Mem"|awk '{print $3}'))"
S="$(sensors|tail -5|sed 's/        / /g'|sed 's/°C.*$//g')"
RS="$R0
────────────
$S"
echo -e "<xml>
<appsettings><tooltip>
$MM
────────────
$RS
────────────"> /tmp/cpuxml
UNITS=$(( $(grep cpu /proc/stat|wc -l) - 1 ))
TOTALCPU="$(echo "$TT"|grep 'cpu '| awk -v RS="" '{printf "%.4f\n", ($13-$2+$15-$4)*100/($13-$2+$15-$4+$16-$5)}'|cut -b -4)"
u=0
while [ $u -lt $UNITS ]
do
	CPU[$u]="$(echo "$TT"|grep 'cpu'$u| awk -v RS="" '{printf "%.0f\n", ($13-$2+$15-$4)*10/($13-$2+$15-$4+$16-$5)}')"
	if [ ${CPU[$u]} -gt 9 ];then	CPU[$u]=9;fi
	CPUX[$u]="$(echo "$TT"|grep 'cpu'$u| awk -v RS="" '{printf "%.3f\n", ($13-$2+$15-$4)*100/($13-$2+$15-$4+$16-$5)}'|cut -b -4)"%
	echo "cpu"$u" : " ${CPUX[$u]}>>/tmp/cpuxml
	((u++))
done
echo "$stats" > "$cache"
echo "</tooltip><clickaction>gnome-system-monitor</clickaction>
</appsettings>
<item><type>box</type>
<attr><vertical>0</vertical>
<xalign>1</xalign>
</attr>   
<item>
<type>text</type>
<value>$TOTALCPU%  </value>
<attr>
<style>font-size: 10pt;font-weight: bold;color: pink</style>
</attr>
</item>">>/tmp/cpuxml
b=0
while [ $b -lt $UNITS ]
do 
	echo "<item>
	<type>box</type>
	<attr><vertical>1</vertical>
	<xalign>0</xalign>
	<yalign>2</yalign>
	<style>padding: 0px</style>
	</attr>	">>		/tmp/cpuxml	
	i=9
	while [ $i -ge 0 ]
	do
		if [ $i -lt ${CPU[$b]} ];then	BAR[$i]="_██████_";else	BAR[$i]="        ";fi
		echo "<item><type>text</type><value>${BAR[$i]}</value><attr><xalign>0</xalign><style>font-size: 1pt;font-weight: bold;color:pink;padding: 0px</style></attr></item>">>/tmp/cpuxml
		((i--))
	done	
 echo "<item>
 <type>text</type>
 <value>_██████_</value>
 <attr>
 <xalign>0</xalign>
 <style>font-size: 1pt;font-weight: bold;color: pink;padding: 0px</style>
 </attr>
 </item>
 </item>">> /tmp/cpuxml
	((b++))
done
echo "</item>
</xml>">>/tmp/cpuxml
cat /tmp/cpuxml

