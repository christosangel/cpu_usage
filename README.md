# cpu_usage

A simple bash script to display cpu usage in **Cinnamon DE**. 

It displays  a percent total usage of CPU.
 
 It also recognizes how many cores there are, and displays one bar for each cpu core in the Cinnamon panel. It works in combination with the **CommandRunner** applet.

![image 1](screenshot1.png)

On hover over the applet, this tooltip is shown:

![image 2](screenshot2.png)

Clicking on the panel applet opens gnome-system-monitor.

## INSTRUCTIONS

-  Change Directory to cpu_usage

`$ cd cpu_usage`

-  Make this file executable with the following command in terminal:

`chmod +x cpu_usage.sh`

* Make sure you download the **CommandRunner Applet** in cinnamon panel:

 * Right click on the panel you want to present the network monitor

 * Select **Applets**

 * Select **Download** tab, select **CommandRunner** and download it

 * Back on the **Manage** tab, press the CommandRunner **Configure** button.

 * In the **Command** field, put : /homer/user'sname/path-to-directory/cpu_usage/cpu_usage.sh
 * In the **Run Interval** field, put: 1 second
 * Hit **Apply**
 * You are good to go.
